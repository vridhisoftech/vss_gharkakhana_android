package com.android.gharkakhana.data.cache.entity

import com.android.gharkakhana.data.model.Article

data class ArticleModel(
        var id: Long?,
        var author: String?,
        val content: String?,
        val description: String?,
        val publishedAt: String?,
        var source: SourceModel?,
        val title: String?,
        val url: String?,
        val urlToImage: String?,
        val selected: Boolean?
)

fun ArticleModel.toDomain() = Article(
        id = id!!,
        author = author,
        content = content,
        description = description,
        publishedAt = publishedAt,
        source = source?.toDomain(),
        title = title,
        url = url,
        urlToImage = urlToImage,
        selected = selected
)

fun ArticleModel.toLocal() = ArticleEntity (
        id = id!!,
        author = author,
        content = content,
        description = description,
        publishedAt = publishedAt,
        name = source?.name,
        title = title,
        url = url,
        urlToImage = urlToImage,
        selected = selected
)