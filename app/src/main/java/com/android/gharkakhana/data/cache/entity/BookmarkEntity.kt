package com.android.gharkakhana.data.cache.entity

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "Bookmark")
data class BookmarkEntity(
        @PrimaryKey(autoGenerate = true) var id: Long = 0,
        val author: String?,
        val content: String?,
        val description: String?,
        val publishedAt: String?,
        val title: String?,
        val url: String?,
        val name: String?,
        val urlToImage: String?,
        val selected: Boolean?
)

fun BookmarkEntity.toData() = BookmarkModel(
        id= id,
        author = author,
        content = content,
        description = description,
        publishedAt = publishedAt,
        source = SourceModel(name),
        title = title,
        url = url,
        urlToImage = urlToImage,
        selected = selected
)