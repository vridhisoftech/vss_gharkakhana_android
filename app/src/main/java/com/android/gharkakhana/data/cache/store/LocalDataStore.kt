package com.android.gharkakhana.data.cache.store

import com.android.gharkakhana.data.cache.dao.AppDao
import com.android.gharkakhana.data.cache.dao.BookmarkDao
import com.android.gharkakhana.data.cache.entity.*
import com.android.gharkakhana.data.model.Article
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject

class LocalDataStore @Inject constructor(
    private val appDao: AppDao,
    private val bookmarkDao: BookmarkDao,
    private val prefHelper: PrefHelper
) {

    companion object {
        private const val EXPIRATION_TIME = (60 * 10 * 1000).toLong()
    }

    suspend fun getArticles(): List<ArticleModel> = withContext(Dispatchers.IO) {
        appDao.getArticles().map { it.toData() }
    }

    suspend fun getArticle(articleId: Long): ArticleModel = withContext(Dispatchers.IO) {
        appDao.getArticleId(articleId).toData()
    }

    suspend fun saveArticles(articles: List<ArticleModel>): List<ArticleModel> =
        withContext(Dispatchers.IO) {
            appDao.deleteArticles()
            appDao.insertArticles(articles.map { it.toLocal() })
            setLastCacheTime(System.currentTimeMillis())
            appDao.getArticles().map { it.toData() }
        }

    suspend fun getBookmarkArticles(): List<BookmarkModel> = withContext(Dispatchers.IO) {
        bookmarkDao.getAllBookmark().map { it.toData() }
    }

    suspend fun saveBookmarkArticle(article: Article) = withContext(Dispatchers.IO) {
        val bookmarkEntity = BookmarkEntity(
            article.id,
            article.author,
            article.content,
            article.description,
            article.publishedAt,
            article.title,
            article.url,
            article.source?.name,
            article.urlToImage,
            true
        )
        bookmarkDao.insertBookmarkArticle(bookmarkEntity)
    }

    suspend fun deleteBookmarkArticle(articleId: Long) = withContext(Dispatchers.IO) {
        bookmarkDao.deleteBookmarkArticle(articleId)
    }

    suspend fun isBookmarksExist(articleId: Long): Boolean = withContext(Dispatchers.IO){
        val article: BookmarkEntity? = bookmarkDao.getBookmarkArticle(articleId)
        article?.id != null
    }

    private fun setLastCacheTime(lastCache: Long) {
        prefHelper.lastCacheTime = lastCache
    }

    private fun getLastCacheUpdateTimeMillis(): Long {
        return prefHelper.lastCacheTime
    }

    fun isExpired(): Boolean {
        val currentTime = System.currentTimeMillis()
        val lastUpdateTime = this.getLastCacheUpdateTimeMillis()
        return currentTime - lastUpdateTime > EXPIRATION_TIME
    }
}