package com.android.gharkakhana.data.cache.dao

import androidx.room.*
import com.android.gharkakhana.data.cache.entity.ArticleEntity

@Dao
interface AppDao {

    /*Main Dao*/
    @Query("SELECT * FROM Article")
    suspend fun getArticles(): List<ArticleEntity>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertArticles(articles: List<ArticleEntity>)

    @Query("SELECT * FROM Article WHERE id = :articleId")
    suspend fun getArticleId(articleId: Long): ArticleEntity

    @Query("DELETE FROM Article")
    suspend fun deleteArticles()

}
