package com.android.gharkakhana.data.repository

import java.util.*

val COUNTRIES_AVAILABLE = arrayOf("ae", "ar", "at", "au", "be", "bg", "br",
        "ca", "ch", "cn", "co", "cu", "cz", "de", "eg", "fr", "gb", "gr", "hk", "hu", "id",
        "ie", "il", "in", "it", "jp", "kr", "lt", "lv", "ma", "mx", "my", "ng", "nl", "no",
        "nz", "ph", "pl", "pt", "ro", "rs", "ru", "sa", "se", "sg", "si", "sk", "th", "tr",
        "tw", "ua", "us", "ve", "za")

data class Specification(
     var category: String,
     var country : String = Locale.getDefault().country.toLowerCase(Locale.ROOT),
     var language: String,
)