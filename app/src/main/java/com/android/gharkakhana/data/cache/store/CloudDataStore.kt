package com.android.gharkakhana.data.cache.store

import com.android.gharkakhana.data.api.ApiService
import com.android.gharkakhana.data.cache.entity.ArticleModel
import com.android.gharkakhana.data.cache.entity.toData
import com.android.gharkakhana.exception.NotFoundException
import com.android.gharkakhana.utils.NetworkHelper
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject

class CloudDataStore @Inject constructor(
    private val api: ApiService,
    private val networkHelper: NetworkHelper
) {

    suspend fun getArticles(): List<ArticleModel> = withContext(Dispatchers.IO) {
        if (networkHelper.isInternetOn()) {
            api.getArticles(category = "", country = "in", pageSize = "100")
                .let { response ->
                    response.body()?.articles?.map { it.toData() } ?: throw NotFoundException()
                }
        } else {
            arrayListOf()
        }
    }

    suspend fun getDiscovers(cat: String): List<ArticleModel> = withContext(Dispatchers.IO) {
        if (networkHelper.isInternetOn()) {
            api.getArticles(category = cat, country = "in", pageSize = "100")
                .let { response ->
                    response.body()?.articles?.map { it.toData() } ?: throw NotFoundException()
                }
        } else {
            arrayListOf()
        }
    }
}