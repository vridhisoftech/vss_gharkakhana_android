package com.android.gharkakhana.data.cache.dao

import androidx.room.*
import com.android.gharkakhana.data.cache.entity.BookmarkEntity

@Dao
interface BookmarkDao {

    @Query("SELECT * FROM Bookmark")
    suspend fun getAllBookmark(): List<BookmarkEntity>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertBookmarkArticle(article: BookmarkEntity)

    @Query("DELETE FROM Bookmark WHERE id=:articleId")
    suspend fun deleteBookmarkArticle(articleId: Long)

    @Query("SELECT * FROM Bookmark WHERE id = :articleId")
    suspend fun getBookmarkArticle(articleId: Long): BookmarkEntity
}