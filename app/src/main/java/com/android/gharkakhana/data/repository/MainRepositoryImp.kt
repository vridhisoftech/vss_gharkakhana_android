package com.android.gharkakhana.data.repository

import com.android.gharkakhana.data.cache.entity.toDomain
import com.android.gharkakhana.data.cache.store.CloudDataStore
import com.android.gharkakhana.data.cache.store.LocalDataStore
import com.android.gharkakhana.data.model.Article
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject

class MainRepositoryImp @Inject constructor(
    private val cloudDataStore: CloudDataStore,
    private val localDataStore: LocalDataStore
) : MainRepository {

    override suspend fun getArticlesList(refresh: Boolean): List<Article> = withContext(Dispatchers.IO) {
        val userLocalList = getUsersFromLocal()
        if (userLocalList.isNotEmpty() && !refresh) {
            if (localDataStore.isExpired()) {
                getUsersFromRemote()
            } else {
                getUsersFromLocal()
            }
        } else {
            getUsersFromRemote()
        }
    }

    private suspend fun getUsersFromRemote(): List<Article> = withContext(Dispatchers.IO) {
        val usersFromRemote = cloudDataStore.getArticles()
        if (usersFromRemote.isNotEmpty()) {
            localDataStore.saveArticles(usersFromRemote).map { it.toDomain() }
        } else {
            getUsersFromLocal()
        }
    }

    override suspend fun getDiscoverArticle(cat: String): List<Article> = withContext(Dispatchers.IO) {
        cloudDataStore.getDiscovers(cat).map { it.toDomain() }
    }

    private suspend fun getUsersFromLocal(): List<Article> = withContext(Dispatchers.IO) {
        localDataStore.getArticles().map { it.toDomain() }
    }

    override suspend fun getArticle(articleId: Long): Article = withContext(Dispatchers.IO) {
        localDataStore.getArticle(articleId).toDomain()
    }

    override suspend fun deleteArticle(article: Article) {
        //TODO("Not yet implemented")
    }

    override suspend fun saveBookmarkArticle(article: Article) {
        localDataStore.saveBookmarkArticle(article)
    }

    override suspend fun getBookmarkArticle(): List<Article> = withContext(Dispatchers.IO){
        localDataStore.getBookmarkArticles().map { it.toDomain() }
    }

    override suspend fun deleteBookmarkArticle(articleId: Long) = withContext(Dispatchers.IO){
        localDataStore.deleteBookmarkArticle(articleId)
    }

    override suspend fun isBookmarksExist(articleId: Long): Boolean = withContext(Dispatchers.IO){
        localDataStore.isBookmarksExist(articleId)
    }

}