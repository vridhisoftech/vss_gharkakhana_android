package com.android.gharkakhana.data.cache.entity

import androidx.room.PrimaryKey
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class ArticleResponse(
    @PrimaryKey(autoGenerate = true) var id: Long = 0,
    @SerialName("source") val source: SourceResponse,
    @SerialName("author") val author: String?,
    @SerialName("title") val title: String?,
    @SerialName("description") val description: String?,
    @SerialName("url") val url: String?,
    @SerialName("urlToImage") val urlToImage: String?,
    @SerialName("publishedAt") val publishedAt: String?,
    @SerialName("content") val content: String?
)

fun ArticleResponse.toData() = ArticleModel(
        id = id,
        source = source.toData(),
        author = author,
        content = content,
        description = description,
        publishedAt = publishedAt,
        title = title,
        url = url,
        urlToImage = urlToImage,
        selected = false
)