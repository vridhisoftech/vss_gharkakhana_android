package com.android.gharkakhana.data.cache.entity

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "Article")
data class ArticleEntity(
        @PrimaryKey(autoGenerate = true) var id: Long = 0,
        val author: String?,
        val content: String?,
        val description: String?,
        val publishedAt: String?,
        val title: String?,
        val url: String?,
        val name: String?,
        val urlToImage: String?,
        val selected: Boolean?
)

fun ArticleEntity.toData() = ArticleModel(
        id = id,
        author = author,
        content = content,
        description = description,
        publishedAt = publishedAt,
        source = SourceModel(name),
        title = title,
        url = url,
        urlToImage = urlToImage,
        selected = selected
)