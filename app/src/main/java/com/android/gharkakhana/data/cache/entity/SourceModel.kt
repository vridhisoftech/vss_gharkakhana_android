package com.android.gharkakhana.data.cache.entity

import com.android.gharkakhana.data.model.Source

data class SourceModel(
    val name: String?
)

fun SourceModel.toDomain() = Source(
    name = name
)