package com.android.gharkakhana.data.model

open class BaseResponse<T> {
    var articles: T? = null
    var status: String? = null
    var totalResults: Int? = 0
}