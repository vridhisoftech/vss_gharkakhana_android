package com.android.gharkakhana.data.cache.entity

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class SourceResponse(
    @SerialName("name") val name: String?
)

fun SourceResponse.toData() = SourceModel(
    name = name
)