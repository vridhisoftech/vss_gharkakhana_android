package com.android.gharkakhana.data.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Plans(
    val title: String,
    val subTitle: String,
    val thumb: String,
) : Parcelable
