package com.android.gharkakhana.utils

import android.graphics.Rect
import android.view.View
import android.widget.ImageView
import androidx.core.content.ContextCompat
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.android.gharkakhana.R
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.google.android.material.snackbar.BaseTransientBottomBar
import com.google.android.material.snackbar.Snackbar
import java.text.SimpleDateFormat
import java.util.*

object ViewBindingAdapters {

    @JvmStatic
    fun formatDate(publish: String?): String? {
        return publish?.let {
            val date = SimpleDateFormat("dd-MM-yyyy").parse(publish)
            val format = SimpleDateFormat("dd MMM", Locale.getDefault())
             format.format(Date(date.time))
        }

    }

    @JvmStatic
    fun truncateExtra(content: String?): String? {
        return content?.replace("(\\[\\+\\d+ chars])".toRegex(), "") ?: ""
    }

    @JvmStatic
    @BindingAdapter("visible")
    fun setVisible(view: View, visible: Boolean) {
        view.visibility = if (visible) View.VISIBLE else View.INVISIBLE
    }

    @JvmStatic
    @BindingAdapter("showLongMessage", "callback", requireAll = false)
    fun showLongMessage(
        view: View,
        text: String?,
        callback: BaseTransientBottomBar.BaseCallback<Snackbar>? = null
    ) {
        text?.let {
            val snackbar = Snackbar.make(view, it, Snackbar.LENGTH_LONG)
            if (callback != null) snackbar.addCallback(callback)
            snackbar.show()
        }
    }

    @JvmStatic
    @BindingAdapter("loadUrl")
    fun loadUrl(imageView: ImageView, url: String?) {
        url?.let {
            Glide.with(imageView.context)
                .load(it)
                .apply(RequestOptions.noTransformation())
                .into(imageView)
        }
    }

    @JvmStatic
    fun getSourceAndTime(sourceName: String, publish: String?): String? {
        return publish?.let {
            "$sourceName • " + getElapsedTime(
                SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'").parse(publish).time
            )
        }
    }

   @JvmStatic
   @BindingAdapter("setImageResourse")
    fun setImageResourse(imageView: ImageView, isSelected: Boolean) {
       if(isSelected) {
           imageView.setImageDrawable(ContextCompat.getDrawable(imageView.context, R.drawable.ic_saved))
       } else {
           imageView.setImageDrawable(ContextCompat.getDrawable(imageView.context, R.drawable.ic_save))
       }
    }

    @JvmStatic
    fun getElapsedTime(utcTimeString: Long): String? {
        var timeElapsedInSeconds = (System.currentTimeMillis() - utcTimeString) / 1000
        return if (timeElapsedInSeconds < 60) {
            "less than 1m"
        } else if (timeElapsedInSeconds < 3600) {
            timeElapsedInSeconds = timeElapsedInSeconds / 60
            timeElapsedInSeconds.toString() + "m"
        } else if (timeElapsedInSeconds < 86400) {
            timeElapsedInSeconds = timeElapsedInSeconds / 3600
            timeElapsedInSeconds.toString() + "h"
        } else {
            timeElapsedInSeconds = timeElapsedInSeconds / 86400
            timeElapsedInSeconds.toString() + "d"
        }
    }
}