package com.android.gharkakhana.utils

import android.graphics.Rect
import android.view.View
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView

class GridSpacingDecoration(private val spacing: Int) : RecyclerView.ItemDecoration() {
    override fun getItemOffsets(
        outRect: Rect,
        view: View,
        parent: RecyclerView,
        state: RecyclerView.State
    ) {
        val layoutManager = parent.layoutManager as? GridLayoutManager
        if (layoutManager == null || layoutManager.orientation != GridLayoutManager.VERTICAL) {
            return super.getItemOffsets(outRect, view, parent, state)
        }

        val spanCount = layoutManager.spanCount
        val position = parent.getChildAdapterPosition(view)
        val column = position % spanCount
        with(outRect) {
            left = if (column == 0) 0 else spacing / 2
            right = if (column == spanCount.dec()) 0 else spacing / 2
            top = if (position < spanCount) 0 else spacing
        }
    }
}