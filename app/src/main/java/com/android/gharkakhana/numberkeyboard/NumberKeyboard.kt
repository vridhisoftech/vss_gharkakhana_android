package com.android.gharkakhana.numberkeyboard

import android.content.Context
import android.graphics.Typeface
import android.util.AttributeSet
import android.util.TypedValue
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.AttrRes
import androidx.annotation.ColorRes
import androidx.annotation.Dimension
import androidx.annotation.DrawableRes
import androidx.appcompat.app.AppCompatDelegate
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import com.android.gharkakhana.R
import java.util.*

/**
 * Number keyboard (to enter pin or custom amount).
 */
class NumberKeyboard : ConstraintLayout {
    companion object {
        private const val DEFAULT_KEY_WIDTH_DP = -1 // match_parent
        private const val DEFAULT_KEY_HEIGHT_DP = -1 // match_parent
        private const val DEFAULT_KEY_PADDING_DP = 16

        init {
            AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)
        }
    }

    @Dimension
    private var keyWidth = 0

    @Dimension
    private var keyHeight = 0

    @Dimension
    private var keyPadding = 0

    @DrawableRes
    private var numberKeyBackground = 0

    @ColorRes
    private var numberKeyTextColor = 0

    @DrawableRes
    private var leftAuxBtnIcon = 0

    @DrawableRes
    private var leftAuxBtnBackground = 0

    @DrawableRes
    private var rightAuxBtnIcon = 0

    @DrawableRes
    private var rightAuxBtnBackground = 0
    private lateinit var numericKeys: MutableList<TextView>
    private var leftAuxBtn: ImageView? = null
    private var rightAuxBtn: ImageView? = null
    private var listener: NumberKeyboardListener? = null
    private var type = 0

    constructor(context: Context) : super(context) {
        inflateView()
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        initializeAttributes(attrs)
        inflateView()
    }

    constructor(context: Context, attrs: AttributeSet?, @AttrRes defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    ) {
        initializeAttributes(attrs)
        inflateView()
    }

    /**
     * Sets keyboard listener.
     */
    fun setListener(listener: NumberKeyboardListener?) {
        this.listener = listener
    }

    /**
     * Hides left auxiliary button.
     */
    fun hideLeftAuxButton() {
        leftAuxBtn!!.visibility = GONE
    }

    /**
     * Shows left auxiliary button.
     */
    fun showLeftAuxButton() {
        leftAuxBtn!!.visibility = VISIBLE
    }

    /**
     * Hides right auxiliary button.
     */
    fun hideRightAuxButton() {
        rightAuxBtn!!.visibility = GONE
    }

    /**
     * Shows right auxiliary button.
     */
    fun showRightAuxButton() {
        rightAuxBtn!!.visibility = VISIBLE
    }

    /**
     * Sets key width in px.
     */
    fun setKeyWidth(px: Int) {
        if (px == DEFAULT_KEY_WIDTH_DP) {
            return
        }
        for (key in numericKeys) {
            key.layoutParams.width = px
        }
        leftAuxBtn!!.layoutParams.width = px
        rightAuxBtn!!.layoutParams.width = px
        requestLayout()
    }

    /**
     * Sets key height in px.
     */
    fun setKeyHeight(px: Int) {
        if (px == DEFAULT_KEY_HEIGHT_DP) {
            return
        }
        for (key in numericKeys) {
            key.layoutParams.height = px
        }
        leftAuxBtn!!.layoutParams.height = px
        rightAuxBtn!!.layoutParams.height = px
        requestLayout()
    }

    /**
     * Sets key padding in px.
     */
    fun setKeyPadding(px: Int) {
        for (key in numericKeys) {
            key.setPadding(px, px, px, px)
            key.compoundDrawablePadding = -1 * px
        }
        leftAuxBtn!!.setPadding(px, px, px, px)
        rightAuxBtn!!.setPadding(px, px, px, px)
    }

    /**
     * Sets number keys background.
     */
    fun setNumberKeyBackground(@DrawableRes background: Int) {
        for (key in numericKeys) {
            key.background = ContextCompat.getDrawable(context, background)
        }
    }

    /**
     * Sets number keys text color.
     */
    fun setNumberKeyTextColor(@ColorRes color: Int) {
        for (key in numericKeys) {
            key.setTextColor(ContextCompat.getColorStateList(context, color))
        }
    }

    /**
     * Sets number keys text typeface.
     */
    fun setNumberKeyTypeface(typeface: Typeface?) {
        for (key in numericKeys) {
            key.typeface = typeface
        }
    }

    /**
     * Sets left auxiliary button icon.
     */
    fun setLeftAuxButtonIcon(@DrawableRes icon: Int) {
        leftAuxBtn!!.setImageResource(icon)
    }

    /**
     * Sets right auxiliary button icon.
     */
    fun setRightAuxButtonIcon(@DrawableRes icon: Int) {
        rightAuxBtn!!.setImageResource(icon)
    }

    /**
     * Sets left auxiliary button background.
     */
    fun setLeftAuxButtonBackground(@DrawableRes bg: Int) {
        leftAuxBtn!!.background = ContextCompat.getDrawable(context, bg)
    }

    /**
     * Sets right auxiliary button background.
     */
    fun setRightAuxButtonBackground(@DrawableRes bg: Int) {
        rightAuxBtn!!.background = ContextCompat.getDrawable(context, bg)
    }

    /**
     * Initializes XML attributes.
     */
    private fun initializeAttributes(attrs: AttributeSet?) {
        val array = context.theme.obtainStyledAttributes(
            attrs, R.styleable.NumberKeyboard, 0, 0
        )
        try {
            // Get keyboard type
            type = array.getInt(R.styleable.NumberKeyboard_keyboardType, -1)
            require(type != -1) { "keyboardType attribute is required." }
            // Get key sizes
            keyWidth =
                array.getLayoutDimension(R.styleable.NumberKeyboard_keyWidth, DEFAULT_KEY_WIDTH_DP)
            keyHeight = array.getLayoutDimension(
                R.styleable.NumberKeyboard_keyHeight,
                DEFAULT_KEY_HEIGHT_DP
            )
            // Get key padding
            keyPadding = array.getDimensionPixelSize(
                R.styleable.NumberKeyboard_keyPadding,
                dpToPx(DEFAULT_KEY_PADDING_DP.toFloat())
            )
            // Get number key background
            numberKeyBackground = array.getResourceId(
                R.styleable.NumberKeyboard_numberKeyBackground,
                R.drawable.key_bg
            )
            // Get number key text color
            numberKeyTextColor = array.getResourceId(
                R.styleable.NumberKeyboard_numberKeyTextColor,
                R.drawable.key_text_color
            )
            when (type) {
                0 -> {
                    leftAuxBtnIcon = R.drawable.key_bg_transparent
                    rightAuxBtnIcon = R.drawable.ic_backspace
                    leftAuxBtnBackground = R.drawable.key_bg_transparent
                    rightAuxBtnBackground = R.drawable.key_bg_transparent
                }
                1 -> {
                    leftAuxBtnIcon = R.drawable.ic_comma
                    rightAuxBtnIcon = R.drawable.ic_backspace
                    leftAuxBtnBackground = R.drawable.key_bg
                    rightAuxBtnBackground = R.drawable.key_bg_transparent
                }
                2 -> {
                    leftAuxBtnIcon = R.drawable.ic_fingerprint
                    rightAuxBtnIcon = R.drawable.ic_backspace
                    leftAuxBtnBackground = R.drawable.key_bg_transparent
                    rightAuxBtnBackground = R.drawable.key_bg_transparent
                }
                3 -> {
                    leftAuxBtnIcon = array.getResourceId(
                        R.styleable.NumberKeyboard_leftAuxBtnIcon,
                        R.drawable.key_bg_transparent
                    )
                    rightAuxBtnIcon = array.getResourceId(
                        R.styleable.NumberKeyboard_rightAuxBtnIcon,
                        R.drawable.key_bg_transparent
                    )
                    leftAuxBtnBackground = array.getResourceId(
                        R.styleable.NumberKeyboard_leftAuxBtnBackground,
                        R.drawable.key_bg_transparent
                    )
                    rightAuxBtnBackground = array.getResourceId(
                        R.styleable.NumberKeyboard_rightAuxBtnBackground,
                        R.drawable.key_bg_transparent
                    )
                }
                4 -> {
                    leftAuxBtnIcon = R.drawable.key_bg_transparent
                    rightAuxBtnIcon = R.drawable.key_bg_transparent
                    leftAuxBtnBackground = R.drawable.key_bg_transparent
                    rightAuxBtnBackground = R.drawable.key_bg_transparent
                    // Get number key background
                    numberKeyBackground = array.getResourceId(
                        R.styleable.NumberKeyboard_numberKeyBackground,
                        R.drawable.key_bg_airvting_custom
                    )
                }
                else -> {
                    leftAuxBtnIcon = R.drawable.key_bg_transparent
                    rightAuxBtnIcon = R.drawable.key_bg_transparent
                    leftAuxBtnBackground = R.drawable.key_bg
                    rightAuxBtnBackground = R.drawable.key_bg
                }
            }
        } finally {
            array.recycle()
        }
    }

    /**
     * Inflates layout.
     */
    private fun inflateView() {
        val view: View
        // Get numeric keys
        numericKeys = ArrayList()
        if (type != 4) {
            view = inflate(context, R.layout.number_keyboard, this)
        } else {
            view = inflate(context, R.layout.number_keyboard_airvting_custom, this)
            view.findViewById<View>(R.id.key0).visibility = GONE
        }
        numericKeys.add(view.findViewById<View>(R.id.key0) as TextView)
        numericKeys.add(view.findViewById<View>(R.id.key1) as TextView)
        numericKeys.add(view.findViewById<View>(R.id.key2) as TextView)
        numericKeys.add(view.findViewById<View>(R.id.key3) as TextView)
        numericKeys.add(view.findViewById<View>(R.id.key4) as TextView)
        numericKeys.add(view.findViewById<View>(R.id.key5) as TextView)
        numericKeys.add(view.findViewById<View>(R.id.key6) as TextView)
        numericKeys.add(view.findViewById<View>(R.id.key7) as TextView)
        numericKeys.add(view.findViewById<View>(R.id.key8) as TextView)
        numericKeys.add(view.findViewById<View>(R.id.key9) as TextView)
        // Get auxiliary keys
        leftAuxBtn = view.findViewById(R.id.leftAuxBtn)
        rightAuxBtn = view.findViewById(R.id.rightAuxBtn)
        // Set styles
        setStyles()
        // Set listeners
        setupListeners()
    }

    /**
     * Set styles.
     */
    private fun setStyles() {
        setKeyWidth(keyWidth)
        setKeyHeight(keyHeight)
        setKeyPadding(keyPadding)
        setNumberKeyBackground(numberKeyBackground)
        setNumberKeyTextColor(numberKeyTextColor)
        setLeftAuxButtonIcon(leftAuxBtnIcon)
        setLeftAuxButtonBackground(leftAuxBtnBackground)
        setRightAuxButtonIcon(rightAuxBtnIcon)
        setRightAuxButtonBackground(rightAuxBtnBackground)
    }

    /**
     * Setup on click listeners.
     */
    private fun setupListeners() {
        // Set number callbacks
        for (i in numericKeys.indices) {
            val key = numericKeys[i]
            key.setOnClickListener {
                if (listener != null) {
                    listener!!.onNumberClicked(i)
                }
            }
        }
        // Set auxiliary key callbacks
        leftAuxBtn!!.setOnClickListener {
            if (listener != null) {
                listener!!.onLeftAuxButtonClicked()
            }
        }
        rightAuxBtn!!.setOnClickListener {
            if (listener != null) {
                listener!!.onRightAuxButtonClicked()
            }
        }
    }

    /**
     * Utility method to convert dp to pixels.
     */
    fun dpToPx(valueInDp: Float): Int {
        return TypedValue.applyDimension(
            TypedValue.COMPLEX_UNIT_DIP,
            valueInDp,
            resources.displayMetrics
        ).toInt()
    }
}