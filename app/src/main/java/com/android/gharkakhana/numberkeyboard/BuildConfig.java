/**
 * Automatically generated file. DO NOT MODIFY
 */
package com.android.gharkakhana.numberkeyboard;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String LIBRARY_PACKAGE_NAME = "com.android.gharkakhana.numberkeyboard";
  public static final String BUILD_TYPE = "debug";
}
