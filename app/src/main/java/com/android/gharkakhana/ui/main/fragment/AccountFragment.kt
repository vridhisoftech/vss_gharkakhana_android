package com.android.gharkakhana.ui.main.fragment

import android.view.View
import com.android.gharkakhana.R
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class AccountFragment:BaseFragment(R.layout.fragment_account) {
    override fun initData() {

    }

    override fun initView(view: View) {

    }
}