package com.android.gharkakhana.ui.main.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import androidx.recyclerview.widget.RecyclerView
import com.android.gharkakhana.data.model.Article
import com.android.gharkakhana.databinding.CartListItemBinding
import com.android.gharkakhana.databinding.MainListItemBinding
import java.util.*
import kotlin.collections.ArrayList

class CartListAdapter: RecyclerView.Adapter<RecyclerView.ViewHolder>(), Filterable {

    private val articlesList = mutableListOf<Article>()
    private var articlesFilterList = mutableListOf<Article>()

    override fun getItemCount(): Int {
        return articlesFilterList.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val feeder = articlesFilterList[position]
        (holder as ListViewHolder).bind(feeder)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        return ListViewHolder(
            CartListItemBinding.inflate(layoutInflater, parent, false)
        )
    }

    private inner class ListViewHolder(private var applicationBinding: CartListItemBinding) :
        RecyclerView.ViewHolder(applicationBinding.root) {

        fun bind(feed: Article) {
            applicationBinding.item = feed

            itemView.setOnClickListener {
                //command.onCategoryClick(adapterPosition)
                /* val intent = Intent(requireActivity, DetailsActivity::class.java)
                 val gson = Gson()
                 intent.putExtra("feed", gson.toJson(feed))
                 requireActivity.startActivity(intent)*/
            }
        }
    }

    fun updateArticlesList(userList: List<Article>) {
        with(articlesFilterList) {
            clear()
            addAll(userList)
        }

        notifyDataSetChanged()
    }

    override fun getFilter(): Filter {
        return object : Filter() {
            override fun performFiltering(constraint: CharSequence?): FilterResults {
                val charSearch = constraint.toString()
                if (charSearch.isEmpty()) {
                    articlesFilterList = articlesList
                } else {
                    val resultList = ArrayList<Article>()
                    for (row in articlesList) {
                        if (row.title!!.toLowerCase(Locale.ROOT).contains(
                                charSearch.toLowerCase(
                                    Locale.ROOT
                                )
                            )
                        ) {
                            resultList.add(row)
                        }
                    }
                    articlesFilterList = resultList
                }
                val filterResults = FilterResults()
                filterResults.values = articlesFilterList
                return filterResults
            }

            @Suppress("UNCHECKED_CAST")
            override fun publishResults(constraint: CharSequence?, results: FilterResults?) {
                articlesFilterList = results?.values as ArrayList<Article>
                notifyDataSetChanged()
            }

        }
    }
}