package com.android.gharkakhana.ui.main.viewmodel

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.android.gharkakhana.data.model.Article
import com.android.gharkakhana.data.repository.MainRepository
import com.android.gharkakhana.utils.Status
import kotlinx.coroutines.launch

class CommonViewModel @ViewModelInject constructor(
    private val repository: MainRepository
) : ViewModel() {

    private val _bookmarks = MutableLiveData<Status<List<Article>>>()
    val bookmark: LiveData<Status<List<Article>>> get() = _bookmarks

    private val _discover = MutableLiveData<Status<List<Article>>>()
    val discover: LiveData<Status<List<Article>>> get() = _discover

    init {
        fetchBookmarkArticles()
        fetchDiscoverArticles()
    }

    fun fetchDiscoverArticles(){
        fetchDiscoverArticles(null)
    }

    fun fetchDiscoverArticles(cat: String? = "general"){
        viewModelScope.launch {
            try {
               // Log.v("CurrentThread ","${Thread.currentThread().name}")
                _discover.postValue(Status.Loading)
                val bookmarks = repository.getDiscoverArticle(cat!!)
                _discover.postValue(Status.Success(bookmarks))
            }catch (e: Exception){
                _discover.postValue(Status.Error(e))
            }
        }
    }

    fun fetchBookmarkArticles() {
        viewModelScope.launch {
            try {
                _bookmarks.postValue(Status.Loading)
                val bookmarks = repository.getBookmarkArticle()
                _bookmarks.postValue(Status.Success(bookmarks))
            }catch (e: Exception){
                _bookmarks.postValue(Status.Error(e))
            }
        }
    }

    fun deleteBookmark(id: Long){
        viewModelScope.launch {
            try {
                repository.deleteBookmarkArticle(id)
            }catch (e: java.lang.Exception){

            }
        }
    }
}