package com.android.gharkakhana.ui.main.adapter

import android.net.Uri
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager2.widget.ViewPager2
import com.android.gharkakhana.R
import com.android.gharkakhana.data.model.Article
import com.android.gharkakhana.data.model.Plans
import com.android.gharkakhana.databinding.*
import com.android.gharkakhana.utils.GridSpacingDecoration
import java.util.*
import kotlin.collections.ArrayList


class CommonAdapter constructor(
    private val listener: CommonAdapterListener,
    private var requireActivity: FragmentActivity
) : RecyclerView.Adapter<RecyclerView.ViewHolder>(),
    Filterable {

    companion object {
        const val VIEW_TYPE_PAGER = 0
        const val VIEW_TYPE_GRID = 1
        const val VIEW_TYPE_MAIN = 2
        const val VIEW_TYPE_CUISINE = 3
    }


    private val articlesList = mutableListOf<Article>()
    private var articlesFilterList = mutableListOf<Article>()
    private val gridItems = mutableListOf<Plans>()

    init {
        gridItems.add(
            0,
            Plans(
                "One Time",
                "Food is any substance consumed to provide nutritional support for an organism.",
                Uri.parse("android.resource://com.android.gharkakhana/" + R.drawable.daily).toString()
            )
        )
        gridItems.add(
            1,
            Plans(
                "Weekly",
                "Food is any substance consumed to provide nutritional support for an organism.",
                Uri.parse("android.resource://com.android.gharkakhana/" + R.drawable.weekly).toString()
            )
        )
        gridItems.add(
            2,
            Plans(
                "Monthly",
                "Food is any substance consumed to provide nutritional support for an organism.",
                Uri.parse("android.resource://com.android.gharkakhana/" + R.drawable.monthly).toString()
            )
        )
        gridItems.add(
            3,
            Plans(
                "More than one Month",
                "Food is any substance consumed to provide nutritional support for an organism.",
                Uri.parse("android.resource://com.android.gharkakhana/" + R.drawable.morethen).toString()
            )
        )
    }

    interface CommonAdapterListener {
        fun onMoreClick(article: Article)
        fun onItemClick(article: Article)
    }

    private inner class ViewHolderPager(private var applicationBinding: PagerListViewBinding) :
        RecyclerView.ViewHolder(applicationBinding.root) {

        var currentPage = 0
        val DELAY_MS: Long = 2000 //delay in milliseconds before task is to be executed
        val PERIOD_MS: Long = 12000 // time in milliseconds between successive task executions.
        var handler: Handler? = Handler()
        var timer: Timer? = Timer()

        fun bind(feed: List<Article>) {
            val mainPagerAdapter = PagerListAdapter(
                feed,
                requireActivity
            )
            applicationBinding.viewPager.orientation = ViewPager2.ORIENTATION_HORIZONTAL
            applicationBinding.viewPager.adapter = mainPagerAdapter
            timer?.schedule(object : TimerTask() { // task to be scheduled
                override fun run() {
                    handler?.post(Runnable {
                        if (feed.isEmpty())
                            return@Runnable
                        if (currentPage == feed.size - 1) {
                            currentPage = 0
                        }
                        applicationBinding.viewPager.setCurrentItem(currentPage++, true)
                    })
                }
            }, DELAY_MS, PERIOD_MS)
        }
    }

    private inner class ViewHolderGrid(private var binding: GridListViewBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(feed: List<Plans>) {
            val adapter = GridListAdapter(
                feed,
                requireActivity
            )
            if(feed.isEmpty()){
                binding.text.visibility = View.GONE
            }else binding.text.visibility =View.VISIBLE

            val layoutManager = GridLayoutManager(requireActivity, 2)
            layoutManager.orientation = GridLayoutManager.VERTICAL
            binding.recyclerView.layoutManager = layoutManager
            binding.recyclerView.adapter = adapter
            binding.recyclerView.setHasFixedSize(true)
            binding.recyclerView.addItemDecoration(GridSpacingDecoration(10))
        }
    }

    private inner class ViewHolderMain(private val binding: MainListViewBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(feed: List<Article>) {
            val adapter = MainListAdapter(
                feed,
                requireActivity
            )
            val categoryLinearLayoutManager = GridLayoutManager(requireActivity, 1)
            categoryLinearLayoutManager.orientation = GridLayoutManager.VERTICAL
            binding.recyclerView.layoutManager = categoryLinearLayoutManager
            binding.recyclerView.adapter = adapter
            binding.recyclerView.setHasFixedSize(true)
            binding.recyclerView.addItemDecoration(GridSpacingDecoration(10))
        }
    }

    private inner class ViewHolderCusine(private val binding: CusineListViewBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(feed: List<Article>) {
            val adapter = CusineListAdapter(
                feed,
                requireActivity
            )
            if(feed.isEmpty()){
                binding.text.visibility = View.GONE
            }else binding.text.visibility =View.VISIBLE

            val categoryLinearLayoutManager = GridLayoutManager(requireActivity, 4)
            categoryLinearLayoutManager.orientation = GridLayoutManager.VERTICAL
            binding.recyclerView.layoutManager = categoryLinearLayoutManager
            binding.recyclerView.adapter = adapter
            binding.recyclerView.setHasFixedSize(true)
            binding.recyclerView.addItemDecoration(GridSpacingDecoration(10))
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)

        if (viewType == VIEW_TYPE_PAGER) {
            return ViewHolderPager(
                PagerListViewBinding.inflate(layoutInflater, parent, false)
            )
        }

        if (viewType == VIEW_TYPE_GRID) {
            return ViewHolderGrid(
                GridListViewBinding.inflate(layoutInflater, parent, false)
            )
        }

        if (viewType == VIEW_TYPE_MAIN) {
            return ViewHolderMain(
                MainListViewBinding.inflate(layoutInflater, parent, false)
            )
        }

        return ViewHolderCusine(
            CusineListViewBinding.inflate(layoutInflater, parent, false)
        )
    }

    override fun onBindViewHolder(vh: RecyclerView.ViewHolder, position: Int) {
        when (getItemViewType(position)) {
            VIEW_TYPE_PAGER -> {
                val holder = vh as ViewHolderPager
                holder.bind(articlesFilterList)
            }
            VIEW_TYPE_GRID -> {
                val holder = vh as ViewHolderGrid
                holder.bind(gridItems)
            }
            VIEW_TYPE_MAIN -> {
                val holder = vh as ViewHolderMain
                holder.bind(articlesFilterList)
            }
            VIEW_TYPE_CUISINE -> {
                val holder = vh as ViewHolderCusine
                holder.bind(articlesFilterList)
            }
        }
    }

    override fun getItemCount(): Int = 4

    override fun getItemViewType(position: Int): Int {
        return when (position) {
            0 -> {
                VIEW_TYPE_PAGER
            }
            1 -> {
                VIEW_TYPE_GRID
            }
            2 -> {
                VIEW_TYPE_MAIN
            }
            else -> {
                VIEW_TYPE_CUISINE
            }
        }
    }

    /* override fun onViewRecycled(holder: ViewHolderMain) {
         super.onViewRecycled(holder)
         holder.itemView.setOnClickListener(null)
     }*/

    fun updateArticlesList(userList: List<Article>) {
        with(articlesFilterList) {
            clear()
            addAll(userList)
        }

        notifyDataSetChanged()
    }

    override fun getFilter(): Filter {
        return object : Filter() {
            override fun performFiltering(constraint: CharSequence?): FilterResults {
                val charSearch = constraint.toString()
                if (charSearch.isEmpty()) {
                    articlesFilterList = articlesList
                } else {
                    val resultList = ArrayList<Article>()
                    for (row in articlesList) {
                        if (row.title!!.toLowerCase(Locale.ROOT).contains(
                                charSearch.toLowerCase(
                                    Locale.ROOT
                                )
                            )) {
                            resultList.add(row)
                        }
                    }
                    articlesFilterList = resultList
                }
                val filterResults = FilterResults()
                filterResults.values = articlesFilterList
                return filterResults
            }

            @Suppress("UNCHECKED_CAST")
            override fun publishResults(constraint: CharSequence?, results: FilterResults?) {
                articlesFilterList = results?.values as ArrayList<Article>
                notifyDataSetChanged()
            }

        }
    }

}