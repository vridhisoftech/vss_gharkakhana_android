package com.android.gharkakhana.ui.main.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.android.gharkakhana.data.model.Article
import com.android.gharkakhana.databinding.CartListItemBinding

class OutletDetailListAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var articlesFilterList = mutableListOf<Article>()

    override fun getItemCount(): Int {
        return articlesFilterList.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val feeder = articlesFilterList[position]
        (holder as ListViewHolder).bind(feeder)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        return ListViewHolder(
            CartListItemBinding.inflate(layoutInflater, parent, false)
        )
    }

    private inner class ListViewHolder(private var applicationBinding: CartListItemBinding) :
        RecyclerView.ViewHolder(applicationBinding.root) {

        fun bind(feed: Article) {
            applicationBinding.item = feed

            itemView.setOnClickListener {
                //command.onCategoryClick(adapterPosition)
                /* val intent = Intent(requireActivity, DetailsActivity::class.java)
                 val gson = Gson()
                 intent.putExtra("feed", gson.toJson(feed))
                 requireActivity.startActivity(intent)*/
            }
        }
    }

    fun updateArticlesList(userList: List<Article>) {
        with(articlesFilterList) {
            clear()
            addAll(userList)
        }

        notifyDataSetChanged()
    }
}