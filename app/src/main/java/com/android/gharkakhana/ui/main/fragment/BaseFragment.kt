package com.android.gharkakhana.ui.main.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.ViewCompat
import androidx.core.view.updatePadding
import androidx.fragment.app.Fragment
import timber.log.Timber


/**
 * Fragments that can be lazy loaded are implemented using the androidx fragment, which is different from the traditional way
 */
abstract class BaseFragment constructor(layoutResId: Int) : Fragment(layoutResId) {

    private var mIsInitData = false

    //1
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        if (!isLazyLoad()) {
            fetchData()
        }
        return super.onCreateView(inflater, container, savedInstanceState)
    }

    //2
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        ViewCompat.setOnApplyWindowInsetsListener(view) { v, windowInsets ->
            v.updatePadding(top = windowInsets.systemWindowInsetTop)
            windowInsets.consumeSystemWindowInsets()
        }
        initView(view)
    }

    override fun onResume() {
        super.onResume()
        fetchData()
        val simpleName = javaClass.simpleName
        Timber.d("BaseFragment $simpleName onResume")
    }

    override fun onPause() {
        super.onPause()
        val simpleName = javaClass.simpleName
        Timber.d("BaseFragment $simpleName onPause")
    }

    override fun onHiddenChanged(hidden: Boolean) {
        super.onHiddenChanged(hidden)
        val simpleName = javaClass.simpleName
        Timber.d("BaseFragment $simpleName onHiddenChanged $hidden")
    }

    private fun fetchData() {
        if (mIsInitData) return
        initData()
        mIsInitData = true
    }

    //1
    abstract fun initData()

    //2
    abstract fun initView(view: View)

    /**
     * Whether lazy loading
     */
     open fun isLazyLoad(): Boolean {
        return false
    }
}
