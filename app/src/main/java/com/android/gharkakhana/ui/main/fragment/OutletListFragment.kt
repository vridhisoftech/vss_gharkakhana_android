package com.android.gharkakhana.ui.main.fragment

import android.view.View
import androidx.appcompat.widget.SearchView
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import com.android.gharkakhana.R
import com.android.gharkakhana.data.model.Article
import com.android.gharkakhana.databinding.FragmentOutletListVewBinding
import com.android.gharkakhana.ui.main.adapter.OutletListAdapter
import com.android.gharkakhana.ui.main.viewmodel.OutletViewModel
import com.android.gharkakhana.utils.GridSpacingDecoration
import com.android.gharkakhana.utils.Status
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class OutletListFragment : BaseFragment(R.layout.fragment_outlet_list_vew),
    SearchView.OnQueryTextListener {

    private var adapter: OutletListAdapter? = null

    private val viewModel: OutletViewModel by viewModels()

    private lateinit var mBinding: FragmentOutletListVewBinding

    override fun initView(view: View) {
        val binding = FragmentOutletListVewBinding.bind(view)
        mBinding = binding
        setupUi()
    }

    override fun initData() {
        setupObserver()
    }

    private fun setupUi() {
        mBinding.toolbar.setNavigationOnClickListener { view ->
            view.findNavController().navigateUp()
        }
        this.let { it ->
            if (adapter == null) {
                adapter = OutletListAdapter(requireActivity())
                mBinding.searchView.setOnQueryTextListener(this@OutletListFragment)
            }
            mBinding.apply {
                viewModel = it.viewModel
                mBinding.recyclerView.setHasFixedSize(true)
                val layoutManager = GridLayoutManager(requireContext(), 1)
                layoutManager.orientation = GridLayoutManager.VERTICAL
                mBinding.recyclerView.layoutManager = layoutManager
                mBinding.recyclerView.adapter = adapter
                mBinding.recyclerView.setHasFixedSize(true)
                mBinding.recyclerView.addItemDecoration(GridSpacingDecoration(10))
            }
        }
    }

    private fun setupObserver() {
        viewModel.articles.observe(viewLifecycleOwner, Observer { status ->
            when (status) {
                is Status.Loading -> {
                    mBinding.swipeContainer.isRefreshing = true
                    displayView(true, mBinding.progressBar)
                    displayView(false, mBinding.warning)
                }
                is Status.Error -> {
                    displayError(status.exception.message)
                    mBinding.swipeContainer.isRefreshing = false
                    displayView(true, mBinding.warning)
                    displayView(false, mBinding.progressBar)
                }
                is Status.Success -> {
                    renderList(status.data)
                    mBinding.swipeContainer.isRefreshing = false
                    displayView(false, mBinding.warning)
                    displayView(false, mBinding.progressBar)
                }
            }
        })
    }

    private fun displayError(message: String?) {
        if (message != null) mBinding.warning.text = message else mBinding.warning.text =
            "Unknown error."
    }

    private fun displayView(isDisplayed: Boolean, view: View) {
        view.visibility = if (isDisplayed) View.VISIBLE else View.GONE
    }

    private fun renderList(articles: List<Article>) {
        adapter?.updateArticlesList(articles)
        adapter?.notifyDataSetChanged()
    }

    override fun onQueryTextSubmit(query: String?): Boolean {
        return false
    }

    override fun onQueryTextChange(newText: String?): Boolean {
        adapter?.filter?.filter(newText)
        return false
    }
}