package com.android.gharkakhana.ui.main.fragment

import android.content.Intent
import android.os.CountDownTimer
import android.view.View
import android.view.animation.AlphaAnimation
import android.view.animation.Animation
import androidx.navigation.findNavController
import com.android.gharkakhana.R
import com.android.gharkakhana.databinding.FragmentOtpBinding
import com.android.gharkakhana.numberkeyboard.NumberKeyboardListener
import com.android.gharkakhana.ui.main.activity.AuthActivity
import com.android.gharkakhana.ui.main.activity.HomeActivity
import com.android.gharkakhana.ui.main.viewmodel.AuthViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class OtpFragment : BaseFragment(R.layout.fragment_otp), NumberKeyboardListener {

    private val TIME_COUNT_DOWN = 1 * 60 * 1000
    private lateinit var viewModel: AuthViewModel
    private lateinit var mBinding: FragmentOtpBinding
    var timer: CountDownTimer? = null


    override fun initView(view: View) {
        val binding = FragmentOtpBinding.bind(view)
        mBinding = binding
        setupUi()
    }

    override fun initData() {
        // setupObserver()
    }


    private fun setupUi() {
        mBinding.numberKeyboard.setListener(this)
        mBinding.pinEntryEditText.setAnimateText(true)
        startCountDownTimer()
        mBinding.signIn.setOnClickListener {
            val intent = Intent(requireActivity(), HomeActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
            startActivity(intent)
            requireActivity().finish()
        }

        mBinding.tvResendCode.setOnClickListener {

        }

        mBinding.close.setOnClickListener { view ->
            view.findNavController().navigateUp()
        }
    }

    private fun startCountDownTimer() {
        mBinding.tvResendCode.isEnabled = false
        timer = object : CountDownTimer(
            TIME_COUNT_DOWN.toLong(),
            1000
        ) {
            override fun onTick(millisUntilFinished: Long) {
                try {
                    val min = millisUntilFinished / 1000 / 60
                    val sec = millisUntilFinished / 1000 % 60
                    mBinding.tvTimeResendCode.text =
                        (if (min < 10) "0$min" else min).toString() + ":" + if (sec < 10) "0$sec" else sec
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onFinish() {
                mBinding.tvTimeResendCode.text = "00:00"
                mBinding.tvTimeResendCode.animation = null
                mBinding.tvResendCode.isEnabled = true
            }
        }.start()
        val anim: Animation = AlphaAnimation(0.0f, 1.0f)
        anim.duration = 500 //You can manage the blinking time with this parameter
        anim.startOffset = 0
        anim.repeatMode = Animation.REVERSE
        anim.repeatCount = Animation.INFINITE
        mBinding.tvTimeResendCode.startAnimation(anim)
    }

    override fun onNumberClicked(number: Int) {
        showAmount(number.toString() + "")
    }

    override fun onLeftAuxButtonClicked() {
        //leave empty
    }

    override fun onRightAuxButtonClicked() {
        val length: Int = mBinding.pinEntryEditText.text.length
        if (length > 0) {
            mBinding.pinEntryEditText.text.delete(length - 1, length)
        }
    }

    private fun showAmount(number: String) {
        mBinding.pinEntryEditText.requestFocus()
        mBinding.pinEntryEditText.append(number)
    }

}