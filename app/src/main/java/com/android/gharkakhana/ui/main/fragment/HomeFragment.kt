package com.android.gharkakhana.ui.main.fragment

import android.content.Intent
import android.view.View
import androidx.appcompat.widget.SearchView
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.GridLayoutManager
import com.android.gharkakhana.R
import com.android.gharkakhana.data.model.Article
import com.android.gharkakhana.databinding.FragmentHomeBinding
import com.android.gharkakhana.ui.main.adapter.CommonAdapter
import com.android.gharkakhana.ui.main.viewmodel.MainViewModel
import com.android.gharkakhana.utils.Status
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class HomeFragment : BaseFragment(R.layout.fragment_home),
    CommonAdapter.CommonAdapterListener, SearchView.OnQueryTextListener {

    private var adapter: CommonAdapter? = null

    private val viewModel: MainViewModel by viewModels()

    private lateinit var mBinding: FragmentHomeBinding

    override fun initView(view: View) {
        val binding = FragmentHomeBinding.bind(view)
        mBinding = binding
        setupUi()
    }

    override fun initData() {
        setupObserver()
    }

    override fun onMoreClick(article: Article) {

    }

    override fun onItemClick(article: Article) {
        //  val action = HomeFragmentDirections.actionStoryViewFragmentToWebViewFragment(param = article)
        //  findNavController().navigate(action)
    }

    private fun setupShareButton(article: Article) {
        val intent = Intent(Intent.ACTION_SEND)
        val shareText: String = article.title.toString() + "\n" + article.url
        intent.putExtra(Intent.EXTRA_TEXT, shareText)
        intent.type = "text/plain"
        startActivity(intent)
    }


    private fun setupUi() {
        this.let { it ->
            if (adapter == null) {
                adapter = CommonAdapter(this, requireActivity())
                mBinding.searchView.setOnQueryTextListener(this@HomeFragment)
            }
            mBinding.apply {
                viewModel = it.viewModel
                mBinding.recyclerView.setHasFixedSize(true)
                mBinding.recyclerView.recycledViewPool.setMaxRecycledViews(0, 4)
                mBinding.recyclerView.setItemViewCacheSize(4)
                val layoutManager = GridLayoutManager(requireContext(), 1)
                layoutManager.orientation = GridLayoutManager.VERTICAL
                mBinding.recyclerView.layoutManager = layoutManager
                mBinding.recyclerView.adapter = adapter
            }
        }
    }

    private fun setupObserver() {
        viewModel.articles.observe(viewLifecycleOwner, Observer { status ->
            when (status) {
                is Status.Loading -> {
                    mBinding.swipeContainer.isRefreshing = true
                    displayView(true, mBinding.progressBar)
                    displayView(false, mBinding.warning)
                }
                is Status.Error -> {
                    displayError(status.exception.message)
                    mBinding.swipeContainer.isRefreshing = false
                    displayView(true, mBinding.warning)
                    displayView(false, mBinding.progressBar)
                }
                is Status.Success -> {
                    renderList(status.data)
                    mBinding.swipeContainer.isRefreshing = false
                    displayView(false, mBinding.warning)
                    displayView(false, mBinding.progressBar)
                }
            }
        })
    }

    private fun displayError(message: String?) {
        if (message != null) mBinding.warning.text = message else mBinding.warning.text =
            "Unknown error."
    }

    private fun displayView(isDisplayed: Boolean, view: View) {
        view.visibility = if (isDisplayed) View.VISIBLE else View.GONE
    }

    private fun renderList(articles: List<Article>) {
        adapter?.updateArticlesList(articles)
        adapter?.notifyDataSetChanged()
    }

    override fun onQueryTextSubmit(query: String?): Boolean {
        return false
    }

    override fun onQueryTextChange(newText: String?): Boolean {
        adapter?.filter?.filter(newText)
        return false
    }
}