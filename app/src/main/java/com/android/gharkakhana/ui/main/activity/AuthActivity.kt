package com.android.gharkakhana.ui.main.activity

import android.os.Bundle
import androidx.navigation.findNavController
import com.android.gharkakhana.R
import com.android.gharkakhana.databinding.ActivityAuthBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class AuthActivity : BaseActivity() {

    private lateinit var binding: ActivityAuthBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        hideSystemUI()
        binding = ActivityAuthBinding.inflate(layoutInflater)
        setContentView(binding.root)
    }

    /**
     * Called on first creation and when restoring state.
     */
    override fun onSupportNavigateUp(): Boolean {
        return findNavController(R.id.nav_host_container).navigateUp()
    }
}