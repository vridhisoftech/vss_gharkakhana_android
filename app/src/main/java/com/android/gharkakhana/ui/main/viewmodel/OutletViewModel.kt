package com.android.gharkakhana.ui.main.viewmodel

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.android.gharkakhana.data.model.Article
import com.android.gharkakhana.data.repository.MainRepository
import com.android.gharkakhana.utils.Status
import kotlinx.coroutines.launch

class OutletViewModel @ViewModelInject constructor(
    private val repository: MainRepository
) : ViewModel() {

    private val _articles = MutableLiveData<Status<List<Article>>>()
    val articles: LiveData<Status<List<Article>>> get() = _articles

    init {
        fetchData()
    }

    fun fetchData(refresh: Boolean = false) {
        fetchArticles(refresh)
    }

    private fun fetchArticles(refresh: Boolean) {
        viewModelScope.launch {
            try {
                _articles.postValue(Status.Loading)
                val articles = repository.getArticlesList(refresh)
                _articles.postValue(Status.Success(articles))
            } catch (e: Exception) {
                _articles.postValue(Status.Error(e))
            }
        }
    }
}