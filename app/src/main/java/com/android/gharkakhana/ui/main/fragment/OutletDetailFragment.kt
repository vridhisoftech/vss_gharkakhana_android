package com.android.gharkakhana.ui.main.fragment

import android.view.View
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import com.android.gharkakhana.R
import com.android.gharkakhana.data.model.Article
import com.android.gharkakhana.databinding.FragmentCartBinding
import com.android.gharkakhana.databinding.FragmentOutletDetailBinding
import com.android.gharkakhana.ui.main.adapter.CartListAdapter
import com.android.gharkakhana.ui.main.adapter.OutletDetailListAdapter
import com.android.gharkakhana.ui.main.viewmodel.OutletViewModel
import com.android.gharkakhana.utils.GridSpacingDecoration
import com.android.gharkakhana.utils.Status
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class OutletDetailFragment : BaseFragment(R.layout.fragment_outlet_detail) {

    private var adapter: OutletDetailListAdapter? = null

    private val viewModel: OutletViewModel by viewModels()

    private lateinit var mBinding: FragmentOutletDetailBinding

    override fun initView(view: View) {
        val binding = FragmentOutletDetailBinding.bind(view)
        mBinding = binding
        setupUi()
    }

    override fun initData() {
        setupObserver()
    }

    private fun setupUi() {
        mBinding.toolbar.setNavigationOnClickListener { view ->
            view.findNavController().navigateUp()
        }
        this.let { it ->
            if (adapter == null) {
                adapter = OutletDetailListAdapter()
            }
            mBinding.apply {
                viewModel = it.viewModel
                mBinding.recyclerView.setHasFixedSize(true)
                val layoutManager = GridLayoutManager(requireContext(), 1)
                layoutManager.orientation = GridLayoutManager.VERTICAL
                mBinding.recyclerView.layoutManager = layoutManager
                mBinding.recyclerView.adapter = adapter
                mBinding.recyclerView.setHasFixedSize(true)
                mBinding.recyclerView.addItemDecoration(GridSpacingDecoration(10))
            }
        }
    }

    private fun setupObserver() {
        viewModel.articles.observe(viewLifecycleOwner, Observer { status ->
            when (status) {
                is Status.Loading -> {
                    displayView(true, mBinding.progressBar)
                    displayView(false, mBinding.warning)
                }
                is Status.Error -> {
                    displayError(status.exception.message)
                    displayView(true, mBinding.warning)
                    displayView(false, mBinding.progressBar)
                }
                is Status.Success -> {
                    renderList(status.data)
                    displayView(false, mBinding.warning)
                    displayView(false, mBinding.progressBar)
                }
            }
        })
    }

    private fun displayError(message: String?) {
        if (message != null) mBinding.warning.text = message else mBinding.warning.text =
            "Unknown error."
    }

    private fun displayView(isDisplayed: Boolean, view: View) {
        view.visibility = if (isDisplayed) View.VISIBLE else View.GONE
    }

    private fun renderList(articles: List<Article>) {
        adapter?.updateArticlesList(articles)
        adapter?.notifyDataSetChanged()
    }
}