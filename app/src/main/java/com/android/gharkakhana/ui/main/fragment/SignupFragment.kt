package com.android.gharkakhana.ui.main.fragment

import android.view.View
import androidx.navigation.findNavController
import com.android.gharkakhana.R
import com.android.gharkakhana.databinding.FragmentSignupBinding

class SignupFragment : BaseFragment(R.layout.fragment_signup) {

    private lateinit var mBinding: FragmentSignupBinding

    override fun initView(view: View) {
        val binding = FragmentSignupBinding.bind(view)
        mBinding = binding
        setupUi()
    }

    private fun setupUi() {
        mBinding.close.setOnClickListener { view ->
            view.findNavController().navigateUp()
        }
    }

    override fun initData() {

    }

}