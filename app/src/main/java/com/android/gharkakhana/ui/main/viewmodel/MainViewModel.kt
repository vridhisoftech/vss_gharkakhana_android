package com.android.gharkakhana.ui.main.viewmodel

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.android.gharkakhana.data.model.Article
import com.android.gharkakhana.data.repository.MainRepository
import com.android.gharkakhana.utils.Status
import kotlinx.coroutines.launch

class MainViewModel @ViewModelInject constructor(
    private val repository: MainRepository
) : ViewModel() {

    private val _articles = MutableLiveData<Status<List<Article>>>()
    val articles: LiveData<Status<List<Article>>> get() = _articles

    private val _bookmarks = MutableLiveData<List<Article>>()
    val bookmark: LiveData<List<Article>> get() = _bookmarks

    init {
        fetchData()
    }

    fun fetchData(refresh: Boolean = false){
        fetchArticles(refresh)
    }

   private fun fetchArticles(refresh: Boolean) {
        viewModelScope.launch {
            try {
                _articles.postValue(Status.Loading)
                val articles = repository.getArticlesList(refresh)
                _articles.postValue(Status.Success(articles))
            } catch (e: Exception) {
                _articles.postValue(Status.Error(e))
            }
        }
    }

    private fun fetchBookmarks(){
        viewModelScope.launch {
            try {
                val bookmarks = repository.getBookmarkArticle()
                _bookmarks.postValue(bookmarks)
            }catch (e: Exception){
                e.printStackTrace()
            }
        }
    }

    fun saveBookmark(article: Article) {
        viewModelScope.launch {
            repository.saveBookmarkArticle(article)
        }
    }

    fun deleteBookmark(articleId: Long) {
        viewModelScope.launch {
            repository.deleteBookmarkArticle(articleId)
        }
    }

}