package com.android.gharkakhana.ui.main.adapter

import android.app.Activity
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.android.gharkakhana.R
import com.android.gharkakhana.data.model.Article
import com.android.gharkakhana.databinding.PagerListItemBinding

class PagerListAdapter(
    private val mCategoryList: List<Article>,
    private val activity: Activity
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun getItemCount(): Int {
        return mCategoryList.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val feeder = mCategoryList[position]
        (holder as RecyclerViewHolder).bind(feeder)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val applicationBinding = PagerListItemBinding.inflate(layoutInflater, parent, false)
        return RecyclerViewHolder(applicationBinding)
    }

    inner class RecyclerViewHolder(private var applicationBinding: PagerListItemBinding) :
        RecyclerView.ViewHolder(applicationBinding.root) {

        fun bind(feed: Article) {
            applicationBinding.article = feed

             itemView.setOnClickListener {
                 activity.findNavController(R.id.nav_host_container).navigate(R.id.outlet_detail_fragment)
             }
        }
    }
}
