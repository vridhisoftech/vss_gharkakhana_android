package com.android.gharkakhana.ui.main.fragment

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.Nullable
import androidx.databinding.DataBindingUtil
import com.android.gharkakhana.R
import com.android.gharkakhana.databinding.FragmentOptionsBottomSheetBinding
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import timber.log.Timber


class OptionsBottomSheet : BottomSheetDialogFragment(), View.OnClickListener {
    private var title: String? = null
    private var url: String? = null
    private var id: Long? = 0
    private var listener: OptionsBottomSheetListener? = null

    override fun onCreate(@Nullable savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (arguments != null) {
            title = arguments?.getString(PARAM_TITLE)
            url = arguments?.getString(PARAM_URL)
            id = arguments?.getLong(PARAM_ID)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val binding: FragmentOptionsBottomSheetBinding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_options_bottom_sheet,
            container,
            false
        )
        binding.btnDelete.setCompoundDrawablesWithIntrinsicBounds(
            R.drawable.ic_delete,
            0,
            0,
            0
        )
        binding.btnOpenInBrowser.setCompoundDrawablesWithIntrinsicBounds(
            R.drawable.ic_open_in_browser,
            0,
            0,
            0
        )
        binding.btnShare.setCompoundDrawablesWithIntrinsicBounds(
            R.drawable.ic_share_gray,
            0,
            0,
            0
        )
        binding.btnShare.setOnClickListener(this)
        binding.btnOpenInBrowser.setOnClickListener(this)
        binding.btnDelete.setOnClickListener(this)
        return binding.root
    }

    override fun onClick(v: View) {
        val intent: Intent
        when (v.id) {
            R.id.btn_open_in_browser -> {
                intent = Intent(Intent.ACTION_VIEW, Uri.parse(url))
                fragment?.dismiss()
                startActivity(intent)
            }
            R.id.btn_share -> {
                val shareText = """
                    $title
                    $url
                    """.trimIndent()
                intent = Intent(Intent.ACTION_SEND)
                intent.putExtra(Intent.EXTRA_TEXT, shareText)
                intent.type = "text/plain"
                this.dismiss()
                startActivity(intent)
            }
            R.id.btn_delete -> {
                listener!!.onRemove(id)
                Timber.d("Saved for id  : %s", id)
                dismiss()
            }
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    interface OptionsBottomSheetListener {
        fun onRemove(id: Long?)
    }

    fun setOnItemClickListner(listener: OptionsBottomSheetListener) {
        this.listener = listener
    }

    companion object {
        private const val PARAM_TITLE = "param-title"
        private const val PARAM_URL = "param-url"
        private const val PARAM_ID = "param-id"
        private var fragment: OptionsBottomSheet? = null
        fun getInstance(title: String?, url: String?, id: Long): OptionsBottomSheet? {
            fragment = OptionsBottomSheet()
            val args = Bundle()
            args.putString(PARAM_TITLE, title)
            args.putString(PARAM_URL, url)
            args.putLong(PARAM_ID, id)
            fragment?.arguments = args
            return fragment
        }
    }
}
