package com.android.gharkakhana.ui.main.fragment

import android.view.View
import androidx.navigation.findNavController
import com.android.gharkakhana.R
import com.android.gharkakhana.databinding.FragmentLoginBinding
import com.android.gharkakhana.databinding.FragmentOtpBinding

class LoginFragment : BaseFragment(R.layout.fragment_login) {

    private lateinit var mBinding: FragmentLoginBinding

    override fun initView(view: View) {
        val binding = FragmentLoginBinding.bind(view)
        mBinding = binding
        setupUi()
    }

    override fun initData() {
        // setupObserver()
    }


    private fun setupUi(){
        mBinding.close.setOnClickListener { view ->
            view.findNavController().navigateUp()
        }

        mBinding.ivSendOtp.setOnClickListener {
            it.findNavController().navigate(R.id.otp_fragment)
        }
        mBinding.btnGotoEmail.setOnClickListener {
            it.findNavController().navigate(R.id.signup_fragment)
        }
    }
}