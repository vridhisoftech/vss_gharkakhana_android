package com.android.gharkakhana.ui.main.fragment

import android.view.View
import androidx.appcompat.widget.SearchView
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import com.android.gharkakhana.R
import com.android.gharkakhana.data.model.Article
import com.android.gharkakhana.databinding.FragmentCartBinding
import com.android.gharkakhana.databinding.FragmentNotificationBinding
import com.android.gharkakhana.ui.main.adapter.CartListAdapter
import com.android.gharkakhana.ui.main.adapter.NotificationListAdapter
import com.android.gharkakhana.ui.main.viewmodel.OutletViewModel
import com.android.gharkakhana.utils.GridSpacingDecoration
import com.android.gharkakhana.utils.Status
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class NotificationFragment: BaseFragment(R.layout.fragment_notification),
    SearchView.OnQueryTextListener {

    private var adapter: NotificationListAdapter? = null

    private val viewModel: OutletViewModel by viewModels()

    private lateinit var mBinding: FragmentNotificationBinding

    override fun initView(view: View) {
        val binding = FragmentNotificationBinding.bind(view)
        mBinding = binding
        setupUi()
    }

    override fun initData() {
        setupObserver()
    }

    private fun setupUi() {
        this.let { it ->
            if (adapter == null) {
                adapter = NotificationListAdapter()
                mBinding.searchView.setOnQueryTextListener(this@NotificationFragment)
            }
            mBinding.apply {
                viewModel = it.viewModel
                mBinding.recyclerView.setHasFixedSize(true)
                val layoutManager = GridLayoutManager(requireContext(), 1)
                layoutManager.orientation = GridLayoutManager.VERTICAL
                mBinding.recyclerView.layoutManager = layoutManager
                mBinding.recyclerView.adapter = adapter
                mBinding.recyclerView.setHasFixedSize(true)
                mBinding.recyclerView.addItemDecoration(GridSpacingDecoration(10))
            }
        }
    }

    private fun setupObserver() {
        viewModel.articles.observe(viewLifecycleOwner, Observer { status ->
            when (status) {
                is Status.Loading -> {
                    mBinding.swipeContainer.isRefreshing = true
                    displayView(true, mBinding.progressBar)
                    displayView(false, mBinding.warning)
                }
                is Status.Error -> {
                    displayError(status.exception.message)
                    mBinding.swipeContainer.isRefreshing = false
                    displayView(true, mBinding.warning)
                    displayView(false, mBinding.progressBar)
                }
                is Status.Success -> {
                    renderList(status.data)
                    mBinding.swipeContainer.isRefreshing = false
                    displayView(false, mBinding.warning)
                    displayView(false, mBinding.progressBar)
                }
            }
        })
    }

    private fun displayError(message: String?) {
        if (message != null) mBinding.warning.text = message else mBinding.warning.text =
            "Unknown error."
    }

    private fun displayView(isDisplayed: Boolean, view: View) {
        view.visibility = if (isDisplayed) View.VISIBLE else View.GONE
    }

    private fun renderList(articles: List<Article>) {
        adapter?.updateArticlesList(articles)
        adapter?.notifyDataSetChanged()
    }

    override fun onQueryTextSubmit(query: String?): Boolean {
        return false
    }

    override fun onQueryTextChange(newText: String?): Boolean {
        adapter?.filter?.filter(newText)
        return false
    }
}