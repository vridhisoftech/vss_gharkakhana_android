buildscript {
    repositories {
        google()
        jcenter()
    }
    dependencies {
        classpath(Dependencies.androidGradlePlugin)
        classpath(Dependencies.kotlinSerializationGradlePugin)
        classpath(Dependencies.hiltAndroidGradlePlugin)
        classpath(Dependencies.navigationSafeArgsGradlePlugin)
        classpath(Dependencies.kotlinGradlePlugin)
    }
}

allprojects {
    repositories {
        google()
        jcenter()
    }
}

tasks.register("clean", Delete::class) {
    delete(rootProject.buildDir)
}
